!===============================================================================
!  create_initial_lagpos
!
!     create an initial particle position file (NetCDF) for Lagrangian tracking
!     users can use this to convert an ascii particle position file or to 
!     generate a grid of particles with a user specified Cartesian box.
!     Initial positions for several groups of particles can be generated.
!     These groups can have separate release and freeze times.
!
! to compile:
!  ifort create_initial_lagpos.f90 -L/usr/local/lib -lnetcdf -I/usr/local/include
!
!===============================================================================

  module particle_list_class

  !particle list type 
  type particle_list
    integer :: nparticles
    real*4  :: tbeg
    real*4  :: tend 
    real*4, pointer :: x(:)
    real*4, pointer :: y(:)
    real*4, pointer :: z(:)
  end type particle_list

  contains

  !particle list constructor
  function particle_list_ (nparticles) result(p)
    integer, intent(in) :: nparticles
    type(particle_list) :: p
    p%nparticles = nparticles
    allocate(p%x(nparticles)) ; p%x = 0.0
    allocate(p%y(nparticles)) ; p%y = 0.0
    allocate(p%z(nparticles)) ; p%z = 0.0
  end function particle_list_

  end module particle_list_class

  !====== Main Program ====================
  program create_initial_particle_file
  use particle_list_class
  implicit none

  integer :: ngroups, nparticles,itype, i, temp
  character(len=120) info_string
  type(particle_list), allocatable :: plist(:)


  !dump general information about the program
  call dump_info

  !setup control parameters
  call setup

  !allocate main data
  allocate(plist(ngroups))

  !begin reading in particle position info
  write(*,*)'we will begin setting up groups'
  select case(itype)

  !-------------------------------------------------
  case(1) !!box
  !-------------------------------------------------

  write(*,*)'we will begin setting up groups'
  do i=1,ngroups
    write(*,*)'setting up group: ',i,' of ',ngroups
    call box_seeder(plist(i))
  end do

  !-------------------------------------------------
  case(2) !!external file
  !-------------------------------------------------

  write(*,*)'we will begin setting up groups'
  write(*,*)'the format of your external file must be:'
  write(*,*)'-----------------------------------'
  write(*,*)'-----------------------------------'
  write(*,*)'np'
  write(*,*)'1  x1   y1   z1'
  write(*,*)'2  x2   y2   z2'
  write(*,*)'.....'
  write(*,*)'np x_np y_np z_np'
  write(*,*)'-----------------------------------'
  write(*,*)'where z is position of the particle' 
  write(*,*)'below the surface and must be <= 0'
  write(*,*)'a value of 0 will place the particle on the'
  write(*,*)'surface'
  write(*,*)'x,y,z are all in meters'

  do i=1,ngroups
    write(*,*)'setting up group: ',i,' of ',ngroups
    call get_list(plist(i))
  end do


  end select

  !dump initial position file in netcdf format
  call dump_initial_file
    

  contains

  subroutine dump_info
    write(*,*)'welcome to "create_initial_particle_file"'
    write(*,*)'use this program to create a NetCDF format file'
    write(*,*)'containing initial particle data'
    write(*,*)
    write(*,*)'You will have the option of specifying a grid of particles'
    write(*,*)'in a box, or reading in an ascii particle position list'
    write(*,*)'created externally'
    write(*,*)
    write(*,*)' you will also be able to set up different groups of particles'
    write(*,*)' which have common properties such as release and end times'
    write(*,*)
    write(*,*)'                      begin   '
    write(*,*)''
  end subroutine dump_info

  subroutine setup
  
    write(*,*)'enter an descriptive sentence about this particle set'
    read(*,'(120A)') info_string
    
    write(*,*)'enter method to describe initial particle locations'
    write(*,*)'box:       (1)'
    write(*,*)'external:  (2)'
    read(*,*) itype
     
    if(itype < 1 .or. itype > 2)then
      write(*,*)'answer should be 1, or 2' 
      stop
    endif
    if(itype == 1)write(*,*)'box it is'
    if(itype == 2)write(*,*)'external it is'
  
    write(*,*)'how many groups will you be setting up'
    read(*,*)ngroups
    if(ngroups < 1)then
      write(*,*)ngroups,' groups does not make any sense'
      stop
    endif

  end subroutine setup

  subroutine box_seeder(pl)
    implicit none
    type(particle_list), intent(inout) :: pl
    real*4 xl,xr,yb,yt,ds,depth,x,y
    integer ii

    write(*,*)'enter xleft  xright  ybottom   ytop    in meters'
    read(*,*)xl,xr,yb,yt
    write(*,*)'enter particle spacing in meters'
    read(*,*)ds
    write(*,*)'enter particle zlocation (0 for surface, < 0 below)' 
    read(*,*)depth


    !count particles
    y = yb
    ii = 0
    do  
      x = xl
      do 
        ii = ii + 1
        x = x + ds
        if (x > xr) exit
      end do
      y = y + ds
      if (y > yt) exit
    end do

    !dump header and initialize list
    write(*,*)'total particles in group: ',ii 
    pl = particle_list_ (ii)

    !set initial particle positions 
    y = yb
    ii = 0
    do
      x = xl
      do
        ii = ii + 1
        pl%x(ii) = x
        pl%y(ii) = y
        pl%z(ii) = depth 
        x = x + ds
        if (x > xr) exit
      end do
      y = y + ds
      if (y > yt) exit
    end do

    !get begin, end times
    write(*,*)'enter particle release time in hours'
    write(*,*)'when the model reaches this time, particles'
    write(*,*)'from this group will be released'
    read(*,*)pl%tbeg
    write(*,*)'enter particle end time'
    write(*,*)'when the model reaches this time, particles'
    write(*,*)'from this group will become frozen'
    read(*,*)pl%tend

  end subroutine box_seeder

    
  subroutine get_list(pl)
    type(particle_list), intent(inout) :: pl
    character(len=120 ) :: fname
    integer :: i,j,ijunk
    logical :: fexist

    write(*,*)'enter ascii filename'
     read(*,*)fname
    write(*,*)'checking for existence of: ',fname
    inquire(exist=fexist,file=fname)
    if(.not.fexist)then
      write(*,*)'file: ',trim(fname),' does not exist'
      write(*,*)'stopping'
    endif

    open(unit=33,file=fname,form='formatted')
    read(33,*)temp
    write(*,*)'reading: ',temp,' particles from file'

    !allocate particle list arrays
    pl = particle_list_ (temp)
    do i=1,pl%nparticles
      read(33,*)ijunk,pl%x(i),pl%y(i),pl%z(i)
    end do
    write(*,*)'read in all particles from: ',trim(fname)

    !get begin, end times
    write(*,*)'enter particle release time in hours'
    write(*,*)'when the model reaches this time, particles'
    write(*,*)'from this group will be released'
    read(*,*)pl%tbeg
    write(*,*)'enter particle end time'
    write(*,*)'when the model reaches this time, particles'
    write(*,*)'from this group will become frozen'
    read(*,*)pl%tend

  end subroutine get_list

  subroutine dump_initial_file

     use netcdf
     integer :: ierr 
     integer :: nc_fid
     integer :: lag_did
     integer :: x_vid
     integer :: y_vid
     integer :: z_vid
     integer :: path_vid
     integer :: tbeg_vid
     integer :: tend_vid
     integer :: group_vid
     integer :: mark_vid
     integer :: pathlength_vid
     integer :: i,j,ii
     real*4, allocatable :: ftmp(:)
     integer,allocatable :: itmp(:)
     character(len=120)  :: fname

     !open netcdf file
     fname = "lag_init.nc" 
     ierr = nf90_create(trim(fname),nf90_64bit_offset,nc_fid)
     if(ierr /= nf90_noerr)then
       write(*,*)'error creating', trim(fname)
       write(*,*)trim(nf90_strerror(ierr))
     endif

     !count global number of particles
     nparticles = 0
     do i=1,ngroups
       nparticles = nparticles + plist(i)%nparticles
     end do

     !global attributes
     ierr = nf90_put_att(nc_fid,nf90_global,"info_string",trim(info_string))
     ierr = nf90_put_att(nc_fid,nf90_global,"dump_counter",0)    
     ierr = nf90_put_att(nc_fid,nf90_global,"t_last_dump",0.0)    
     ierr = nf90_put_att(nc_fid,nf90_global,"number_particles",nparticles)

     !dimensions
     ierr = nf90_def_dim(nc_fid,"nparticles",nparticles, lag_did)

     !-----x---------
     ierr = nf90_def_var(nc_fid,"x",nf90_float,lag_did,x_vid)
     ierr = nf90_put_att(nc_fid,x_vid,"long_name","particle x position")
     ierr = nf90_put_att(nc_fid,x_vid,"units","m")
     !-----y---------
     ierr = nf90_def_var(nc_fid,"y",nf90_float,lag_did,y_vid)
     ierr = nf90_put_att(nc_fid,y_vid,"long_name","particle y position")
     ierr = nf90_put_att(nc_fid,y_vid,"units","m")
     !-----z---------
     ierr = nf90_def_var(nc_fid,"z",nf90_float,lag_did,z_vid)
     ierr = nf90_put_att(nc_fid,z_vid,"long_name","particle z position")
     ierr = nf90_put_att(nc_fid,z_vid,"units","m")
     !-----tbeg------
     ierr = nf90_def_var(nc_fid,"tbeg",nf90_float,lag_did,tbeg_vid)
     ierr = nf90_put_att(nc_fid,tbeg_vid,"long_name","particle release time")
     ierr = nf90_put_att(nc_fid,tbeg_vid,"units","hours")
     !-----tend------
     ierr = nf90_def_var(nc_fid,"tend",nf90_float,lag_did,tend_vid)
     ierr = nf90_put_att(nc_fid,tend_vid,"long_name","particle freeze time")
     ierr = nf90_put_att(nc_fid,tend_vid,"units","hours")
     !-----pathlength
     ierr = nf90_def_var(nc_fid,"pathlength",nf90_float,lag_did,path_vid)
     ierr = nf90_put_att(nc_fid,path_vid,"long_name","particle integrated path length")
     ierr = nf90_put_att(nc_fid,path_vid,"units","m")
     !-----group      
     ierr = nf90_def_var(nc_fid,"group",nf90_int,lag_did,group_vid)
     ierr = nf90_put_att(nc_fid,group_vid,"long_name","particle group")
     ierr = nf90_put_att(nc_fid,group_vid,"units","-")
     !-----mark       
     ierr = nf90_def_var(nc_fid,"mark",nf90_int,lag_did,mark_vid)
     ierr = nf90_put_att(nc_fid,mark_vid,"long_name","particle marker (0=in domain)")
     ierr = nf90_put_att(nc_fid,mark_vid,"units","-")

     !enddef and close
     ierr = nf90_enddef(nc_fid)
     ierr = nf90_close(nc_fid)

     !open and dump
     ierr = nf90_open(fname,nf90_write,nc_fid)
     if(ierr /= nf90_noerr)then
       write(*,*)'error opening', trim(fname)
       write(*,*)trim(nf90_strerror(ierr))
     endif


     !collect and dump x
     allocate(ftmp(nparticles)) ; ftmp = 0.0
     ii = 0
     do i=1,ngroups
       do j=1,plist(i)%nparticles
         ii = ii + 1
         ftmp(ii) = plist(i)%x(j)
       end do
     end do
     ierr = nf90_put_var(nc_fid,x_vid,ftmp) 
     deallocate(ftmp)

     !collect and dump y
     allocate(ftmp(nparticles)) ; ftmp = 0.0
     ii = 0
     do i=1,ngroups
       do j=1,plist(i)%nparticles
         ii = ii + 1
         ftmp(ii) = plist(i)%y(j)
       end do
     end do
     ierr = nf90_put_var(nc_fid,y_vid,ftmp)
     deallocate(ftmp)

     !collect and dump z
     allocate(ftmp(nparticles)) ; ftmp = 0.0
     ii = 0
     do i=1,ngroups
       do j=1,plist(i)%nparticles
         ii = ii + 1
         ftmp(ii) = plist(i)%z(j)
       end do
     end do
     ierr = nf90_put_var(nc_fid,z_vid,ftmp)
     deallocate(ftmp)

     !collect and dump pathlength
     allocate(ftmp(nparticles)) ; ftmp = 0.0
     ierr = nf90_put_var(nc_fid,path_vid,ftmp)
     deallocate(ftmp)

     !collect and dump tbeg 
     allocate(ftmp(nparticles)) ; ftmp = 0.0
     ii = 0
     do i=1,ngroups
       do j=1,plist(i)%nparticles
         ii = ii + 1
         ftmp(ii) = plist(i)%tbeg
       end do
     end do
     ierr = nf90_put_var(nc_fid,tbeg_vid,ftmp)
     deallocate(ftmp)

     !collect and dump tend 
     allocate(ftmp(nparticles)) ; ftmp = 0.0
     ii = 0
     do i=1,ngroups
       do j=1,plist(i)%nparticles
         ii = ii + 1
         ftmp(ii) = plist(i)%tend
       end do
     end do
     ierr = nf90_put_var(nc_fid,tend_vid,ftmp)
     deallocate(ftmp)

     !collect and dump group 
     allocate(itmp(nparticles)) ; itmp = 0.0
     ii = 0
     do i=1,ngroups
       do j=1,plist(i)%nparticles
         ii = ii + 1
         itmp(ii) = i 
       end do
     end do
     ierr = nf90_put_var(nc_fid,group_vid,itmp)
     deallocate(itmp)

     !collect and dump mark 
     allocate(itmp(nparticles)) ; itmp = 0.0
     ierr = nf90_put_var(nc_fid,mark_vid,itmp)
     deallocate(itmp)


     ierr = nf90_close(nc_fid)

 
  end subroutine dump_initial_file

  end program create_initial_particle_file
